

const gui = new dat.GUI();
gui.close()
const canvas = document.querySelector('canvas');
const c = canvas.getContext('2d');
canvas.width = innerWidth;
canvas.height = innerHeight;
const wave = {
    y:canvas.height/2,
    length: .01,
    amplitude: 100,
    frequency: .01,
}
const strokeFill = {
    h:200,
    s:50,
    l:50
}
const bgColor ={
    r: 0,
    g:0,
    b:0,
    a:.01,
}

const waveFolder = gui.addFolder('wave')
waveFolder.add(wave,'y',0,canvas.height)
waveFolder.add(wave,'length',-0.01,0.01)
waveFolder.add(wave,'amplitude',-300,300)
waveFolder.add(wave,'frequency',-.05,.05)
waveFolder.open()

const strokeFolder = gui.addFolder('strokeFill')
strokeFolder.add(strokeFill,'h',0,255)
strokeFolder.add(strokeFill,'s',0,100)
strokeFolder.add(strokeFill,'l',0,100)
strokeFolder.open()
var increament = wave.frequency

const bgColorFolder = gui.addFolder('Background Color')
bgColorFolder.add(bgColor,'r',0,255)
bgColorFolder.add(bgColor,'g',0,255)
bgColorFolder.add(bgColor,'b',0,255)
bgColorFolder.add(bgColor,'a',0,1)
bgColorFolder.open()

function animate(){
    requestAnimationFrame(animate)

    c.fillStyle = `rgba(${bgColor.r},${bgColor.g},${bgColor.b},${bgColor.a})`
    c.fillRect(0,0,canvas.width,canvas.height);
    c.beginPath(0, canvas.height/2)
    for( i=0;i<canvas.width;i++){
        c.lineTo(i,wave.y + Math.sin(i *wave.length + increament)*wave.amplitude *Math.sin(increament))
    }
    c.strokeStyle = `hsl(${Math.abs(strokeFill.h *Math.sin(increament))}, ${strokeFill.s}%, ${strokeFill.l}%)`
    c.stroke()

    increament += wave.frequency
}

animate()