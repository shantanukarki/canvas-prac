var canvas = document.querySelector('canvas')
console.log(canvas)
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

var c = canvas.getContext('2d');




// for(i=0;i < 3; i++){
//     var x = Math.random() * window.innerWidth;
//     var y = Math.random() * window.innerHeight;
//     var colours = ['blue','red','yellow'];
//     var color = colours[Math.floor(Math.random() * colours.length)];

//     c.beginPath()
//     c.moveTo(x,y)
//     c.lineTo(x+100,y+100)
//     c.strokeStyle = color;
//     c.stroke()
// }




// for(i=0;i < 3; i++){
//     var x = Math.random() * window.innerWidth;
//     var y = Math.random() * window.innerHeight;
//     var colours = ['blue','red','yellow'];
//     var color = colours[Math.floor(Math.random() * colours.length)];
//     c.fillStyle = color;
//     c.fillRect(x,y, 100, 100)

// }


// for(i=0;i < 3; i++){
//     var x = Math.random() * innerWidth;
//     var y = Math.random() * innerHeight;
//     var colours = ['blue','red','green'];
//     var color = colours[Math.floor(Math.random() * colours.length)];
//     c.beginPath();
//     var r = 5 ;
//     c.arc(x,y,r,0,Math.PI *2 ,false)
//     c.fillStyle =  color;
//     c.fill()
// }


// var r = 10;
// var x=r;
// var y=r;
// dx = (Math.random() - 0.5)*10;
// dy = (Math.random() - 0.5)*10;
// console.log(dx)
// function animate(){
//     requestAnimationFrame(animate);
//     c.clearRect(0,0,innerWidth,innerHeight)
//     c.beginPath();
//     c.arc(x,y,r,0,Math.PI *2 ,false)
//     c.fillStyle =  'blue';
//     c.fill()
//     if(x + r > innerWidth || x-r < 0){
//         dx = -dx
//         }
//     if(y + r > innerHeight || y - r <0){
//         dy = -dy
//     }
//     x += dx;
//     y += dy;
// }
// animate();

var mouse = {
    x:undefined,
    y:undefined,
}

window.addEventListener('mousemove',function(e){
    mouse.x = e.x;
    mouse.y = e.y;
})
window.addEventListener('resize',function(){
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    init();
})
var maxR = 40;

class Circle  {
    constructor (x,y,dx,dy,r,color){
    this.x = x;
    this.y = y;
    this.mindx=dx;
    this.mindy=dy;
    this.dx = dx;
    this.dy = dy
    this.color = color;
    this.r = r;
    this.minR = r;
    }
    

    draw() {
        c.beginPath();
        c.arc(this.x, this.y, this.r, 0, Math.PI*2 , false)
        c.fillStyle = this.color;
        c.fill()
    }
    
    update(){
        if(this.x + this.r > canvas.width || this.x - this.r < 0){
            this.dx = -this.dx;
        }
        if(this.y + this.r > canvas.height || this.y - this.r < 0){
            this.dy = -this.dy;
        }

        this.x += this.dx;
        this.y += this.dy;

        if(mouse.x - this.x < 50 && mouse.x - this.x > -50 && mouse.y - this.y < 50 && mouse.y - this.y > -50){
            this.r += 1;
        }
        else if(this.r > this.minR){
            this.r -=1;
        }
        if(this.r>maxR){
            this.r -= 1;
        }
        this.draw()
    }
}

var circleArray = [];
function init(){

    
    circleArray = [];

    for ( i=0; i<500;i++){
        var r = Math.random()*10 + 2;
        var x = Math.random() * (canvas.width- r*2) +r;
        var y = Math.random() * (canvas.height -r*2) +r;
        var dx = (Math.random() - 0.5)*5;
        var dy = (Math.random() - 0.5)*5;
        var colours = ['blue','red','yellow','green','black','orange','violet','indigo'];
        var color = colours[Math.floor(Math.random() * colours.length)];
        circleArray.push(new Circle(x , y,dx,dy,r,color))
    }
}
init();
console.log(circleArray)

function animate(){
    requestAnimationFrame(animate);
    c.clearRect(0,0,canvas.width,canvas.height)
    for(i=0 ; i < circleArray.length ; i++){
        circleArray[i].update()
    }
}
animate()