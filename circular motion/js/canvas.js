function randomIntFromRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }
var canvas = document.querySelector('canvas');
canvas.height = innerHeight;
canvas.width = innerWidth;
const c = canvas.getContext('2d');


addEventListener('resize',function(){
    init()
})
const colors = ['#2185C5', '#7ECEFD', '#FF7F66']
let mouse = {
    x : innerWidth/2,
    y : innerHeight/2,
}
addEventListener('mousemove',function(e){
    mouse.x = e.clientX;
    mouse.y = e.clientY
})
console.log(mouse.x,mouse.y)
class Particle {
    constructor(x,y,radius,color){
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.radians= Math.random() * Math.PI*2;
        this.velocity = .05;
        this.distance = randomIntFromRange(50,120);
        this.lastMouse = {
            x:x,
            y:y
        }
    }

    draw(lastPoint){
        c.beginPath();
        c.moveTo(lastPoint.x,lastPoint.y);
        c.lineTo(this.x,this.y)
        c.strokeStyle = this.color;
        c.lineWidth = this.radius
        c.stroke()
    }
    update(){
        const lastPoint ={
            x: this.x,
            y: this.y
        }

        this.radians += this.velocity;
        this.lastMouse.x += (mouse.x - this.lastMouse.x)*.05;
        this.lastMouse.y += (mouse.y - this.lastMouse.y)*.05;
        this.x = this.lastMouse.x + Math.cos(this.radians) * this.distance;
        this.y = this.lastMouse.y +Math.sin(this.radians) * this.distance;
        this.draw(lastPoint)
    }
}


particles = []
function init(){
    particles= [];

    
    for (i = 0; i < 50 ; i++) {
        x= canvas.width/2;
        y= canvas.height/2;
        radius = (Math.random()*2)+1;
        color = colors[Math.floor(Math.random()*colors.length)]
        particles.push(new Particle(x,y,radius,color));
    }
}
function animate(){
    requestAnimationFrame(animate);
    c.fillStyle = 'rgba(0,0,0,.09)';
    c.fillRect(0,0,canvas.width,canvas.height)
    

    for(i=0;i < particles.length;i++ ){
        particles[i].update();
    }
}

init();
animate();