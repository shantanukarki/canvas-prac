// import utils from './utils'

function randomIntFromRange(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min)
}

const canvas = document.querySelector('canvas')
const c = canvas.getContext('2d')

canvas.width = innerWidth
canvas.height = innerHeight


const mouse = {
  x: innerWidth / 2,
  y: innerHeight / 2
}

const colors = ['#2185C5', '#7ECEFD', '#FFF6E5', '#FF7F66']

// Event Listeners
addEventListener('mousemove', (event) => {
  mouse.x = event.clientX
  mouse.y = event.clientY
})

addEventListener('resize', () => {
  canvas.width = innerWidth
  canvas.height = innerHeight

  init()
})
addEventListener('click',() =>{
  init()
})
gravity = 1;

// Objects
class Ball {
  constructor(x, y,dy,dx, radius, color) {
    this.x = x
    this.y = y
    this.dy = dy
    this.dx = dx
    this.radius = radius
    this.color = color
  }

  draw() {
    c.beginPath()
    c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
    c.fillStyle = this.color
    c.fill()
    c.closePath()
  }

  update() {
    if(this.y + this.radius + this.dy > canvas.height ){
      this.dy = -this.dy*.8;
    }
    else{
      this.dy += gravity;
    }

    if (this.x + this.radius + this.dx > canvas.width || this.x -this.radius  < 0){
      this.dx = -this.dx
    }
    this.y += this.dy;
    this.x += this.dx;
    this.draw()
  }
}

// Implementation
let objects
var ballArray = []
var colours = ['red' , 'orange' , 'yellow','green','blue','indigo','violet']
function init() {
  ballArray = []
  for(i=0;i<200;i++){
    var radius = randomIntFromRange(10,20)
    var x = randomIntFromRange(radius*2, (canvas.width-radius*2))
    var y = randomIntFromRange(0, canvas.height/2)
    var color = colours[randomIntFromRange(0, colours.length-1)]
    var dx = randomIntFromRange(-2,2);
    var dy = randomIntFromRange(-2,2);
    ballArray.push(new Ball (x, y,dy,dx,radius,color))
  }
}

// Animation Loop
function animate() {
  requestAnimationFrame(animate)
  c.clearRect(0, 0, canvas.width, canvas.height)

  for(i=0 ; i < ballArray.length; i++){
    ballArray[i].update()
  }
}

init()
animate()
