function randomIntFromRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }
  var media = window.matchMedia("(max-width: 991px)")
  const canvas = document.querySelector('canvas')
  const c = canvas.getContext('2d')
  
  canvas.width = innerWidth
  canvas.height = innerHeight
  
  const mouse = {
    x: innerWidth / 2,
    y: innerHeight / 2
  }
  
  
  // Event Listeners
  addEventListener('mousemove', (event) => {
    mouse.x = event.clientX
    mouse.y = event.clientY
  })
  
  addEventListener('resize', () => {
    canvas.width = innerWidth
    canvas.height = innerHeight
  
    init()
  })
  canvas.addEventListener('click',() =>{
    init()
  })
  gravity = 1;
  
  function getdistance(x1,y1,x2,y2){
      let xD = x2-x1;
      let yD = y2-y1;
      return Math.sqrt(Math.pow(xD,2)+Math.pow(yD,2))
  }
  function rotate(velocity, angle) {
    const rotatedVelocities = {
        x: velocity.x * Math.cos(angle) - velocity.y * Math.sin(angle),
        y: velocity.x * Math.sin(angle) + velocity.y * Math.cos(angle)
    };

    return rotatedVelocities;
  }
  function resolveCollision(particle, otherParticle) {
    const xVelocityDiff = particle.velocity.x - otherParticle.velocity.x;
    const yVelocityDiff = particle.velocity.y - otherParticle.velocity.y;

    const xDist = otherParticle.x - particle.x;
    const yDist = otherParticle.y - particle.y;

    // Prevent accidental overlap of particles
    if (xVelocityDiff * xDist + yVelocityDiff * yDist >= 0) {

        // Grab angle between the two colliding particles
        const angle = -Math.atan2(otherParticle.y - particle.y, otherParticle.x - particle.x);

        // Store mass in var for better readability in collision equation
        const m1 = particle.mass;
        const m2 = otherParticle.mass;

        // Velocity before equation
        const u1 = rotate(particle.velocity, angle);
        const u2 = rotate(otherParticle.velocity, angle);

        // Velocity after 1d collision equation
        const v1 = { x: u1.x * (m1 - m2) / (m1 + m2) + u2.x * 2 * m2 / (m1 + m2), y: u1.y };
        const v2 = { x: u2.x * (m1 - m2) / (m1 + m2) + u1.x * 2 * m2 / (m1 + m2), y: u2.y };

        // Final velocity after rotating axis back to original location
        const vFinal1 = rotate(v1, -angle);
        const vFinal2 = rotate(v2, -angle);

        // Swap particle velocities for realistic bounce effect
        particle.velocity.x = vFinal1.x;
        particle.velocity.y = vFinal1.y;

        otherParticle.velocity.x = vFinal2.x;
        otherParticle.velocity.y = vFinal2.y;
    }
  }
  // Objects
  class Particle {
    constructor(x, y, radius, color) {
      this.x = x;
      this.y = y;
      this.velocity = {
        x: (randomIntFromRange(-2,2))+1,
        y: (randomIntFromRange(-2,2))+1,
      };
      this.radius = radius;
      this.color = color;
      this.mass = Math.pow(this.radius,2)*Math.PI;
      this.opacity = 0;
    }
  
    draw() {
      c.beginPath()
      c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
      c.save()
      c.globalAlpha= this.opacity;
      c.fillStyle =this.color;
      c.fill()
      c.restore()
      c.strokeStyle = this.color
      if(media.matches){
        c.lineWidth = 1;
      }else{
      c.lineWidth = 3;}
      c.stroke()
      c.closePath()
    }
  
    update(particles) {
      if (this.x + this.radius > canvas.width || this.x -this.radius  < 1){
        this.velocity.x = -this.velocity.x
      }
      if (this.y + this.radius  > canvas.height || this.y -this.radius  < 1){
        this.velocity.y = -this.velocity.y
      }

      // if(getdistance(mouse.x,mouse.y,this.x,this.y)<100 && this.opacity < .5){
      //   this.opacity += .01;
      // }
      // else if(this.opacity > 0){
      //   this.opacity -= .01;

      //   this.opacity = Math.max(0,this.opacity)
      // }
      for(i=0;i<particles.length;i++){
        if(this === particles[i]) continue;
        if(getdistance(this.x,this.y,particles[i].x,particles[i].y)- this.radius*2 <0){
          resolveCollision(this,particles[i])
          particles[i].opacity =.5
          this.opacity = 0
        }
      }
      this.x += this.velocity.x;
      this.y += this.velocity.y;
    this.draw()
    }
  }
  
  // Implementation
  var particles = []
  const colours = ['#2185C5', '#7ECEFD', '#FF7F66']
  var Particle1,Particle2;

  function init() {
    particles = []
    for(i=0;i<100;i++){
      if(media.matches){
        var radius = randomIntFromRange(5,8)
      }
      else{
        var radius = randomIntFromRange(10,15)
      }
      let x = randomIntFromRange(radius, (canvas.width-radius))
      let y = randomIntFromRange(radius, (canvas.height-radius))
      var color = colours[randomIntFromRange(0, colours.length-1)]
      if(i !== 0){
          for( let j=0;j < particles.length;j++){
              if(getdistance(x,y,particles[j].x,particles[j].y)- radius*2 <0){
                 x = randomIntFromRange(radius, (canvas.width-radius))
                 y = randomIntFromRange(radius, (canvas.height-radius))
                j = -1
              }
            }
        }
     particles.push(new Particle (x, y,radius,color))
   }
}
  
  // Animation Loop
  function animate() {
    requestAnimationFrame(animate)
    c.clearRect(0, 0, canvas.width, canvas.height)

    for (let i = 0; i < particles.length; i++) {
        particles[i].update(particles);
        console.log(particles[i])
    }
    // Particle1.update()
    // Particle2.x = mouse.x;
    // Particle2.y = mouse.y;
    // Particle2.update()
  }
  
  init()
  animate()
  if(window.length>800){
    console.log('hello  ')
  }