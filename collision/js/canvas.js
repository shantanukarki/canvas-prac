function randomIntFromRange(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
  }
  
  const canvas = document.querySelector('canvas')
  const c = canvas.getContext('2d')
  
  canvas.width = innerWidth
  canvas.height = innerHeight
  
  const mouse = {
    x: innerWidth / 2,
    y: innerHeight / 2
  }
  
  
  // Event Listeners
  addEventListener('mousemove', (event) => {
    mouse.x = event.clientX
    mouse.y = event.clientY
  })
  
  addEventListener('resize', () => {
    canvas.width = innerWidth
    canvas.height = innerHeight
  
    init()
  })
//   addEventListener('click',() =>{
//     init()
//   })
  gravity = 1;
  
  function getdistance(x1,x2,y1,y2){
      let xD = x1-x2;
      let yD = y1-y2;
      return Math.sqrt(Math.pow(xD,2)+Math.pow(yD,2))
  }
  // Objects
  class Ball {
    constructor(x, y,dy,dx, radius, color) {
      this.x = x
      this.y = y
      this.dy = dy
      this.dx = dx
      this.radius = radius
      this.color = color
    }
  
    draw() {
      c.beginPath()
      c.arc(this.x, this.y, this.radius, 0, Math.PI * 2, false)
      c.fillStyle = this.color
      c.fill()
      c.closePath()
    }
  
    update() {
    //   if(this.y + this.radius + this.dy > canvas.height ){
    //     this.dy = -this.dy*.8;
    //   }
    //   else{
    //     this.dy += gravity;
    //   }
  
    //   if (this.x + this.radius + this.dx > canvas.width || this.x -this.radius  < 0){
    //     this.dx = -this.dx
    //   }
    //   this.y += this.dy;
    //   this.x += this.dx;
    
    if(getdistance(ball1.x,ball2.x,ball1.y,ball2.y) < ball1.radius+ball2.radius){
        ball1.color = 'red';
    }
    else{
        ball1.color= 'black';
    }
    this.draw()
    }
  }
  
  // Implementation
  var ballArray = []
  const colours = ['red' , 'orange' , 'yellow','green','blue','indigo','violet']
  var ball1,ball2;

  function init() {
    ballArray = []
    ball1 = new Ball(300,300,0,0,100,'black')
    ball2 = new Ball(undefined,undefined,0,0,50,'red')
    console.log(ball2)
    // for(i=0;i<200;i++){
    //   var radius = randomIntFromRange(10,20)
    //   var x = randomIntFromRange(radius*2, (canvas.width-radius*2))
    //   var y = randomIntFromRange(0, canvas.height/2)
    //   var color = colours[randomIntFromRange(0, colours.length-1)]
    //   var dx = randomIntFromRange(-2,2);
    //   var dy = randomIntFromRange(-2,2);
    //   ballArray.push(new Ball (x, y,dy,dx,radius,color))
    // }
  }
  
  // Animation Loop
  function animate() {
    requestAnimationFrame(animate)
    c.clearRect(0, 0, canvas.width, canvas.height)
  
    ball1.update()
    ball2.x = mouse.x;
    ball2.y = mouse.y;
    ball2.update()
  }
  
  init()
  animate()