function ramdominteger(min,max){
    return Math.floor(Math.random() * (min-max +1)+min)
}
var canvas = document.querySelector('canvas');
canvas.height = innerHeight;
canvas.width = innerWidth;
const c = canvas.getContext('2d');

addEventListener('resize',function(){
    init()
})

let mouse = [
    x = undefined,
    y =undefined,
]
addEventListener('mousemove',function(e){
    mouse.x = e.clientX;
    mouse.y = e.clientY
})

class Ripple {
    constructor(x,y,radius,color){
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.color = color;
        this.radiusIncrease= .2;
    }

    draw(){
        c.beginPath();
        c.arc(this.x,this.y,this.radius,0,Math.PI*2,false);
        c.strokeStyle  = this.color;
        if(this.color === 'transparent'){
            c.lineWidth = 5
        }else{
        c.lineWidth = 1;}
        c.stroke()
    }
    update(){

        if(this.radius > 130){
            this.radius = 100
        }
        this.radius += this.radiusIncrease
        this.draw()
    }
}


rippleArray = []
function init(){
    rippleArray= [];

    x= canvas.width/2;
    y= canvas.height/2;
    radius = 100;
    color= undefined;
    for (i = 0; i < 20 ; i++) {
        rippleArray.push(new Ripple(x,y,radius+2*i,color));
        if(i%4 === 0){
            rippleArray[i].color = 'blue';
        }
        else if((i+1)%4 === 0){
            rippleArray[i].color = 'green';
        }
        else if((i+2)%4 === 0){
            rippleArray[i].color = 'yellow';
        }
        else if((i+3)%4 === 0){
            rippleArray[i].color = 'transparent';
        }
    }
    console.log(rippleArray)
}
function animate(){
    requestAnimationFrame(animate);
    c.clearRect(0,0,canvas.width,canvas.height);

    c.beginPath();
    c.arc(canvas.width/2,canvas.height/2,100,0,Math.PI*2,false);
    c.fillStyle = 'red';
    c.fill()

    for(i=0;i < rippleArray.length;i++ ){
        rippleArray[i].update();
    }
}
init();
animate();